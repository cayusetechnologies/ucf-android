package com.cayusetechnologies.ucf;

import android.content.res.AssetManager;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CustomAdapter
    extends RecyclerArrayAdapter<JSONObject, CustomAdapter.ViewHolder>
    implements StickyRecyclerHeadersAdapter<CustomAdapter.ViewHolder>, Filterable {

  /**
   * The default display for simple information about an activity or event.
   * Views include title, time, location and reminder icon.
   */
  static class ViewHolder extends RecyclerView.ViewHolder {

    private static final List<Long> HEADERS = Arrays.asList(
        1439017200000L,
        1439276400000L,
        1439362800000L,
        1439449200000L,
        1439535600000L,
        1439622000000L,
        1439708400000L);

    protected TextView title, time, location;
    protected ImageView reminderState;

    public ViewHolder(final View view) {
      super(view);
      title = (TextView) view.findViewById(R.id.title);
      time = (TextView) view.findViewById(R.id.time);
      location = (TextView) view.findViewById(R.id.location);
      reminderState = (ImageView) view.findViewById(R.id.reminderState);
    }

  }

  /**
   * The view used to call out a unique event or activity.
   * Includes all ViewHolder views and a image banner. Font size increased.
   */
  static class BannerViewHolder extends ViewHolder {

    protected ImageView banner;

    public BannerViewHolder(final View view) {
      super(view);
      banner = (ImageView) view.findViewById(R.id.banner);
      title.setTextAppearance(view.getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
      time.setTextAppearance(view.getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
      location.setTextAppearance(view.getContext(), android.R.style.TextAppearance_DeviceDefault_Medium);
    }

  }

  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("EEEE, MMM d", Locale.ENGLISH);

  private static final int DEFAULT_VIEW = 0;
  private static final int BANNER_VIEW = 1;

  private final AssetManager assetManager;
  private final JSONObject locations;

  public CustomAdapter(final AssetManager assetManager, final JSONObject locations) {
    this.assetManager = assetManager;
    this.locations = locations;
  }

  @Override
  public Filter getFilter() {
    return new EventsFilter(this);
  }

  @Override
  public int getItemViewType(int position) {
    return getItem(position).has("banner") ? BANNER_VIEW : DEFAULT_VIEW;
  }

  @Override
  public long getHeaderId(int position) {
    return getItemHeaderIndex(position);
  }

  @Override
  public ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
    final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    final View view = inflater.inflate(R.layout.event_header, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    if (viewType == BANNER_VIEW)
      return new BannerViewHolder(inflater.inflate(R.layout.event_item_banner, parent, false));
    return new ViewHolder(inflater.inflate(R.layout.event_item, parent, false));
  }

  @Override
  public void onBindHeaderViewHolder(ViewHolder holder, int position) {
    final TextView textView = (TextView) holder.itemView;
    final Long timestamp = ViewHolder.HEADERS.get(getItemHeaderIndex(position));
    final Date date = new Date(timestamp);
    textView.setText(DATE_FORMAT.format(date));
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    try {
      final JSONObject item = getItem(position);
      final Date timestamp = new Date(item.getLong("timestamp"));
      final String location = item.getString("location");
      holder.title.setText(item.getString("title"));
      holder.time.setText(new SimpleDateFormat("h:mm a", Locale.ENGLISH).format(timestamp));
      holder.location.setText(locations.has(location) ? locations.getJSONObject(location).getString("name") : location);
      holder.reminderState.setVisibility(EventReminder.contains(item) ? View.VISIBLE : View.INVISIBLE);
      if (holder instanceof BannerViewHolder)
        ((BannerViewHolder) holder).banner.setImageBitmap(BitmapFactory.decodeStream(assetManager.open("www/images/" + item.getString("banner"))));
    } catch (final Exception e) {
      Log.e(MainActivity.TAG, "Exception caught while setting item information", e);
    }
  }

  private int getItemHeaderIndex(int position) {
    try {
      final long timestamp = getItem(position).getLong("timestamp");
      for (int i = 1; i < ViewHolder.HEADERS.size(); ++i)
        if (timestamp < ViewHolder.HEADERS.get(i))
          return i - 1;
      Log.w(MainActivity.TAG, "Couldn't find appropriate date header for position " + position);
    } catch (JSONException e) {
      Log.e(MainActivity.TAG, "Exception caught while resolving header id", e);
    }
    return 0;
  }

}
