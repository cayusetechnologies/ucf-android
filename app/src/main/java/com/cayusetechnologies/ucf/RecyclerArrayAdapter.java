package com.cayusetechnologies.ucf;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public abstract class RecyclerArrayAdapter<M, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

  private ArrayList<M> items = new ArrayList<M>();

  public RecyclerArrayAdapter() {
    setHasStableIds(true);
  }

  public void add(final M object) {
    items.add(object);
    notifyDataSetChanged();
  }

  public void add(final int index, final M object) {
    items.add(index, object);
    notifyDataSetChanged();
  }

  public void addAll(final Collection<? extends M> collection) {
    if (collection != null) {
      items.addAll(collection);
      notifyDataSetChanged();
    }
  }

  public void addAll(final M[] items) {
    addAll(Arrays.asList(items));
  }

  public void clear() {
    items.clear();
    notifyDataSetChanged();
  }

  public void remove(final M object) {
    items.remove(object);
    notifyDataSetChanged();
  }

  public M getItem(final int position) {
    return items.get(position);
  }

  public ArrayList<M> getItems() {
    return items;
  }

  @Override
  public long getItemId(final int position) {
    return position;
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

}