package com.cayusetechnologies.ucf;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
public class MainActivity extends AppCompatActivity {

  static final String TAG = "UCF";

  private WebView webView;
  private JSONObject routes;

  @Override
  @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_activity);

    try {
      routes = new JSONObject(readData(getAssets(), "routes"));
    } catch (Exception e) {
      Log.e(MainActivity.TAG, "Exception caught while loading routes data", e);
    }

    final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    setDefaultActionBar();
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        MainActivity.this.onBackPressed();
        webView.requestFocus(View.FOCUS_DOWN | View.FOCUS_UP);
      }
    });

    webView = (WebView) findViewById(R.id.webview);
    final WebSettings settings = webView.getSettings();
    settings.setAllowUniversalAccessFromFileURLs(true);
    settings.setJavaScriptEnabled(true);
    webView.setWebViewClient(new CustomWebViewClient(this));
    webView.loadUrl(getResources().getString(R.string.url));
    webView.requestFocus(View.FOCUS_DOWN | View.FOCUS_UP);

    webView.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        webView.requestFocus();
        webView.setFocusable(true);
      }
    });

    webView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      public void onFocusChange(View arg0, boolean hasFocus) {
        if (hasFocus)
          return;
        webView.requestFocus();
        webView.setFocusable(true);
      }
    });

    webView.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
          case MotionEvent.ACTION_DOWN:
          case MotionEvent.ACTION_UP:
            if (!v.hasFocus())
              v.requestFocus();
            break;
        }
        return false;
      }
    });
  }

  protected void setDefaultActionBar() {
    final ActionBar actionBar = getSupportActionBar();
    if (actionBar == null)
      return;
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setTitle("Umatilla County Fair");
    actionBar.setSubtitle("100 Years, What A Ride!");
    actionBar.setBackgroundDrawable(new ColorDrawable(android.R.color.background_light));
  }

  final List<String> pages = Arrays.asList("admissions", "directions", "rodeo", "parking", "contact", "modal");

  @Override
  public void onBackPressed() {
    final String url = webView.getUrl();
    Log.i(TAG, "URL: " + url);
    if (webView.canGoBack()) {
      webView.goBack();
      return;
    }
    super.onBackPressed();
  }

  class CustomWebViewClient extends WebViewClient {

    private final Pattern pattern = Pattern.compile("native/(\\w+)#?(.+)?");
    private final Activity activity;

    public CustomWebViewClient(final Activity activity) {
      this.activity = activity;
    }

    @Override
    public void onPageFinished(final WebView webView, final String url) {
      Log.i(TAG, "Page finished: " + url);
      webView.evaluateJavascript("(function() { return window.stripHeader(); })();", new ValueCallback<String>() {
        @Override
        public void onReceiveValue(String value) {
          final ActionBar actionBar = MainActivity.this.getSupportActionBar();
          if (actionBar == null)
          return;
          if("null".equals(value)) {
            MainActivity.this.setDefaultActionBar();
            return;
          }
          try {
            final JSONObject args = new JSONObject(value);
            final String[] rgb = args.getString("color").substring(4, args.getString("color").length() - 1).split(", ");
            final int color = Color.rgb(Integer.valueOf(rgb[0]), Integer.valueOf(rgb[1]), Integer.valueOf(rgb[2]));
            actionBar.setBackgroundDrawable(new ColorDrawable(color));
            actionBar.setTitle(Html.fromHtml("<font color='#ffffff'>Umatilla County Fair</font>"));
            actionBar.setSubtitle(Html.fromHtml("<font color='#ffffff'>" + args.getString("title") + "</font>"));
          } catch (JSONException e) {
            e.printStackTrace();
          }
        }
      });
    }

    @Override
    public boolean shouldOverrideUrlLoading(final WebView webView, final String url) {
      if (url.startsWith("tel:")) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(url));
        startActivity(intent);
        return true;
      }

      if (url.startsWith("mailto:")) {
        final Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
        startActivity(Intent.createChooser(emailIntent, "Send e-mail"));
//        final Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{url.substring("mailto:".length())});
//        startActivity(Intent.createChooser(intent, "Send mail"));
        return true;
      }

      final Matcher matcher = pattern.matcher(url);
      if (!matcher.find()) {
        webView.loadUrl(url);
        return true;
      }

      final String action = matcher.group(1);
      final String extra = matcher.group(2);
      if ("events".equals(action)) {
        final Intent intent = new Intent(activity, EventsActivity.class);
        if (extra != null)
          intent.putExtra("extra", extra.replaceAll("%20", " "));
        activity.startActivity(intent);
      } else if ("route".equals(action)) {
        try {
          activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(routes.getString(extra))));
        } catch (Exception e) {
          Log.e(MainActivity.TAG, "Exception caught while reading route data", e);
        }
      }

      webView.goBack();
      return true;
    }

  }

  public static String readData(final AssetManager assets, final String name) throws Exception {
    final StringBuilder result = new StringBuilder();
    final InputStream json = assets.open("www/data/" + name + ".json");
    final BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
    String buffer;
    while ((buffer = in.readLine()) != null)
      result.append(buffer);
    in.close();
    return result.toString();
  }

}
