package com.cayusetechnologies.ucf;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class CustomItemClickListener implements RecyclerView.OnItemTouchListener {

  public interface OnItemClickListener {
    void onItemClick(final View view, final int position);
  }

  private final OnItemClickListener listener;

  private GestureDetector gestureDetector;

  public CustomItemClickListener(final Context context, final OnItemClickListener listener) {
    this.listener = listener;
    gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
      @Override
      public boolean onSingleTapUp(final MotionEvent e) {
        return true;
      }
    });
  }

  @Override
  public boolean onInterceptTouchEvent(final RecyclerView view, final MotionEvent e) {
    final View childView = view.findChildViewUnder(e.getX(), e.getY());
    if (childView != null && listener != null && gestureDetector.onTouchEvent(e))
      listener.onItemClick(childView, view.getChildAdapterPosition(childView));
    return false;
  }

  @Override
  public void onTouchEvent(final RecyclerView view, final MotionEvent motionEvent) {
    //
  }

}
