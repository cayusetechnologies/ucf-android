package com.cayusetechnologies.ucf;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Utility class used to identify if we are in debug mode
 */
public class Debug {

  private static final String APP_PACKAGE = "com.cayusetechnologies.ucf.debug";

  public static boolean isEnabled(final Context context) {
    try {
      final PackageManager packageManager = context.getPackageManager();
      packageManager.getPackageInfo(APP_PACKAGE, PackageManager.GET_ACTIVITIES);
      return packageManager.getApplicationInfo(APP_PACKAGE, 0).enabled;
    } catch (final PackageManager.NameNotFoundException e) {
      return false;
    }
  }

}
