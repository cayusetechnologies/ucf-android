package com.cayusetechnologies.ucf;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

@SuppressLint("CommitPrefEdits")
public class EventReminder extends BroadcastReceiver {

  private static final Set<String> reminders = new HashSet<String>();
  private static final String PREFS_NAME = "EventReminder";

  private static String id(final JSONObject item) {
    try {
      final String title = item.getString("title");
      final long timestamp = item.getLong("timestamp");
      return String.format("%s %d", title, timestamp);
//      return item.toString();
    } catch (JSONException e) {
      Log.e(MainActivity.TAG, "Exception caught while adding reminder", e);
      throw new RuntimeException(e);
    }
  }

  public static boolean contains(final JSONObject item) {
    return reminders.contains(id(item));
  }

  public static void add(final Context context, final JSONObject item) {
    reminders.add(id(item));
    final SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
    final SharedPreferences.Editor edit = prefs.edit();
    edit.putStringSet("reminders", reminders);
    edit.commit();
  }

  public static void remove(final Context context, final JSONObject item) {
    reminders.remove(id(item));
    final SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
    final SharedPreferences.Editor edit = prefs.edit();
    edit.putStringSet("reminders", reminders);
    edit.commit();
  }

  @Override
  public void onReceive(final Context context, final Intent intent) {
    try {
      final String location = intent.getStringExtra("location");
      final JSONObject item = new JSONObject(intent.getStringExtra("item"));

      if(!contains(item))
        return;

      remove(context, item);

      final NotificationCompat.Builder builder =
          new NotificationCompat.Builder(context)
              .setSmallIcon(R.mipmap.ic_launcher)
              .setContentTitle(item.getString("title"))
              .setContentText(location)
              .setAutoCancel(true);

      final Intent notificationIntent = new Intent(context, MainActivity.class);
      PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
      builder.setContentIntent(contentIntent);

      final NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
      manager.notify(1, builder.build());

      final Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
      RingtoneManager.getRingtone(context, notification).play();

//      context.sendBroadcast(new Intent("ReminderDispatch"));
    } catch (JSONException e) {
      Log.e(MainActivity.TAG, "Exception caught while parsing json item", e);
    }
  }

}
