package com.cayusetechnologies.ucf;

import android.util.Log;
import android.widget.Filter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class EventsFilter extends Filter {

  public static boolean is(final String filter, final String type) {
    return filter != null && !"".equals(filter) && filter.split(":")[0].equals(type);
  }

  public static String args(final String filter) {
    return (filter != null && !"".equals(filter)) ? filter.split(":")[1] : "";
  }

  private final RecyclerArrayAdapter<JSONObject, CustomAdapter.ViewHolder> adapter;

  public EventsFilter(final RecyclerArrayAdapter<JSONObject, CustomAdapter.ViewHolder> adapter) {
    this.adapter = adapter;
  }

  @Override
  protected Filter.FilterResults performFiltering(final CharSequence constraint) {
    final ArrayList<JSONObject> result = new ArrayList<>();

    try {
      if (constraint == null) {
        result.addAll(adapter.getItems());
      } else {
        final String filter = constraint.toString();
        if(is(filter, "location")) {
          for (final JSONObject item : adapter.getItems())
            if (args(filter).equals(item.getString("location")))
              result.add(item);
        } else if(is(filter, "tag")) {
          final List<String> tags = Arrays.asList(args(filter).split(","));
          for (final JSONObject item : adapter.getItems()) {
            final JSONArray jsonTags = item.getJSONArray("tags");
            for (int i = 0; i < jsonTags.length(); ++i) {
              if (tags.contains(jsonTags.getString(i)))
                result.add(item);
            }
          }
        }
      }
    } catch (JSONException e) {
      Log.e(MainActivity.TAG, "Exception caught while filtering events", e);
    }

    final FilterResults results = new FilterResults();
    results.values = result;
    results.count = result.size();
    return results;
  }

  @Override
  @SuppressWarnings("unchecked")
  protected void publishResults(CharSequence constraint, FilterResults results) {
    adapter.clear();
    adapter.addAll((Collection<? extends JSONObject>) results.values);
  }

}
