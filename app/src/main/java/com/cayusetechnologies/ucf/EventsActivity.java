/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.cayusetechnologies.ucf;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class EventsActivity extends AppCompatActivity {

  private static final long FIVE_MINUTES = 1000 * 60 * 5;
  public static final String HEADER_COLOR = "rgb(2, 136, 209)";

  //  private final BroadcastReceiver receiver = new EventReminder();
  private JSONObject locations = new JSONObject();
  private RecyclerView recyclerView;

  @Override
//  @SuppressWarnings("ConstantConditions")
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.events_activity);

    final String filter = getIntent().getStringExtra("extra");
    Log.i(MainActivity.TAG, "Events activity started with extras: " + filter);

    try {
      locations = new JSONObject(MainActivity.readData(getAssets(), "locations"));
    } catch (Exception e) {
      Log.e(MainActivity.TAG, "Exception caught while loading locations data", e);
    }

    final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        EventsActivity.this.finish();
      }
    });

    final ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      final String[] rgb = HEADER_COLOR.substring(4, HEADER_COLOR.length() - 1).split(", ");
      final int color = Color.rgb(Integer.valueOf(rgb[0]), Integer.valueOf(rgb[1]), Integer.valueOf(rgb[2]));
      actionBar.setDisplayHomeAsUpEnabled(true);
      actionBar.setBackgroundDrawable(new ColorDrawable(color));
      actionBar.setTitle(Html.fromHtml("<font color='#ffffff'>Umatilla County Fair</font>"));
      try {
        final String subtitle = filter != null && !"".equals(filter) ? getSubtitle(filter) : "Events and Activities";
        actionBar.setSubtitle(Html.fromHtml("<font color='#ffffff'>" + subtitle + "</font>"));
      } catch (JSONException e) {
        Log.e(MainActivity.TAG, "Exception caught while reading location name", e);
      }
    }

    final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

    final CustomAdapter adapter = new CustomAdapter(getAssets(), locations);
    try {
      final JSONArray events = new JSONArray(MainActivity.readData(getAssets(), "events"));
      for (int i = 0; i < events.length(); ++i)
        adapter.add(events.getJSONObject(i));
      adapter.getFilter().filter(filter);
    } catch (final Exception e) {
      Log.e(MainActivity.TAG, "Exception caught while loading events data", e);
    }

    recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(adapter);
    recyclerView.setHasFixedSize(true);
    recyclerView.addItemDecoration(new StickyRecyclerHeadersDecoration(adapter));
    recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
    recyclerView.addOnItemTouchListener(new CustomItemClickListener(this, new CustomItemClickListener.OnItemClickListener() {
      @Override
      public void onItemClick(final View view, final int position) {
        try {
          final CustomAdapter.ViewHolder viewHolder = (CustomAdapter.ViewHolder) recyclerView.findViewHolderForItemId(position);
          try {
            final JSONObject item = adapter.getItem(position);
            final String location = item.getString("location");
            final long timestamp = item.getLong("timestamp") - FIVE_MINUTES;

            final Intent intent = new Intent(EventsActivity.this, EventReminder.class);
            intent.setAction(String.valueOf(System.currentTimeMillis()));
            intent.putExtra("location", locations.has(location) ? locations.getJSONObject(location).getString("name") : location);
            intent.putExtra("item", item.toString());

            final boolean isExisting = viewHolder.reminderState.getVisibility() == View.VISIBLE;
            final String positiveLabel = isExisting ? "Remove" : "Yes";
            final String negativeLabel = isExisting ? "Keep" : "No";
            final String message = isExisting
                ? "You're currently watching this event. Would you like to remove the reminder?"
                : "Would you like to add a reminder for this event?";

            new AlertDialog.Builder(EventsActivity.this)
                .setIcon(R.mipmap.ic_launcher)
                .setTitle(item.getString("title"))
                .setMessage(message)
                .setPositiveButton(positiveLabel, new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(final DialogInterface dialog, final int which) {
                    try {
                      if (isExisting)
                        removeReminder(item);
                      else
                        addReminder(item, intent, timestamp);
                    } finally {
                      adapter.notifyItemChanged(position);
                    }
                  }

                })
                .setNegativeButton(negativeLabel, null)
                .show();
          } catch (final Exception e) {
            Log.e(MainActivity.TAG, "Exception caught while parceling json event", e);
          }

        } catch (final Exception e) {
          Log.e(MainActivity.TAG, "Exception caught while prompting for reminder confirmation", e);
        }
      }
    }));
  }

  private String getSubtitle(final String filter) throws JSONException {
    final String args = EventsFilter.args(filter);
    if (EventsFilter.is(filter, "location"))
      return locations.has(args) ? locations.getJSONObject(args).getString("name") : args;
    else if (EventsFilter.is(filter, "tag"))
      return "Showing only " + args.replace(",", ", ");
    return "";
  }


  @Override
  protected void onPause() {
//    unregisterReceiver(receiver);
    super.onPause();
  }

//    final IntentFilter reminderFilter = new IntentFilter();
//    reminderFilter.setPriority(1);
//    reminderFilter.addAction("ReminderDispatch");
//    registerReceiver(receiver, reminderFilter);

  private void addReminder(JSONObject item, Intent intent, long timestamp) {
    final PendingIntent pendingIntent = PendingIntent.getBroadcast(EventsActivity.this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
    final AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
    final Calendar calendar = new GregorianCalendar(TimeZone.getDefault());

    if (Debug.isEnabled(EventsActivity.this)) {
      calendar.setTimeInMillis(System.currentTimeMillis());
      calendar.add(Calendar.SECOND, 10);
      Toast.makeText(EventsActivity.this, "Debug: Reminder in 10 seconds..", Toast.LENGTH_LONG).show();
    } else {
      calendar.setTimeInMillis(timestamp);
      Toast.makeText(EventsActivity.this, "A reminder has been set. Don't miss it!", Toast.LENGTH_LONG).show();
    }

    EventReminder.add(EventsActivity.this, item);
    alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
  }

  private void removeReminder(JSONObject item) {
    EventReminder.remove(EventsActivity.this, item);
    Toast.makeText(EventsActivity.this, "This reminder has been removed", Toast.LENGTH_LONG).show();
  }

}

//          final JSONArray jsonTags = item.getJSONArray("tags");
//          final String[] tags = new String[jsonTags.length()];
//          for (int i = 0; i < jsonTags.length(); ++i)
//            tags[i] = jsonTags.getString(i);
//                    intent.putExtra("tags", tags);
