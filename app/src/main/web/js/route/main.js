var page = require('../page');
var KenBurns = require('../ken-burns');

var init = function () {
  var containers = page.getCurrent().querySelectorAll('.kenburns');
  for(var i = 0; i < containers.length; ++i) {
    var container = containers[i];
    var delay = parseInt(container.getAttribute('data-delay'));
    new KenBurns(container, delay).start();
  }
  init = function () { /* don't init ken burns again */ };
};

module.exports = function () {
  init();
};