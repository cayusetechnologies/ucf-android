// https://libraries.io/bower/iscroll
var IScroll = require('../../bower_components/iscroll/build/iscroll');
var modal = require('../modal');
var markers = require('../markers');

function handleMarker () {
  if(modal.hide())
    return;
  var path;
  if(this.hasAttribute('data-modal'))
    path = 'modal/' + this.getAttribute('data-modal');
  if(this.hasAttribute('data-location'))
    path = this.getAttribute('data-location');
  console.log('map marker ' + path);
  // window.location.href = "#" + path;
  // history.pushState({}, 'Fairground Map', '#' + path);
  router.setRoute('/' + path);
}


var position;
module.exports = function () {
  position = 0;

  var wrapper = document.getElementById('wrapper'),
      scroller = document.getElementById('scroller'),
      aspect = 1514 / 870;

  // var header = document.querySelector('#page-map > section');
  // wrapper.style.top = header.offsetHeight + 'px';

  var width = wrapper.offsetHeight * aspect;
  scroller.style.width = Math.round(width) + 'px';

  var iscroll = new IScroll(wrapper, {
    tap: true,
    scrollbars: true,
    scrollX: true,
    startX: position,
    bounce: false,
    mouseWheel: true
  });

  iscroll.on('scrollEnd', function () {
    position = this.x;
  });

  var container = wrapper.querySelector('.markers');
  if(container.children.length > 0)
    return;

  for(var i = 0; i < markers.length; ++i) {
    var marker = markers[i];
    var el = document.createElement('div');
    if('modal' in marker)
      el.setAttribute('data-modal', marker.modal);
    if('location' in marker)
      el.setAttribute('data-location', marker.location);
    el.setAttribute("class", "sprite sprite-" + marker.sprite);
    el.style.top = marker.position[0];
    el.style.left = marker.position[1];
    el.addEventListener('click', handleMarker);
    if(isTouchEnabled)
      el.addEventListener('touchstart', handleMarker);
    container.appendChild(el);
  }

  console.log('map initiated');
};