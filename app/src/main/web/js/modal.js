var IScroll = require('../bower_components/iscroll/build/iscroll');

function hide () {
  var modal = document.querySelector('.modal.opened');
  if(modal)
    modal.classList.remove('opened');
  return modal;
}

var handlers = {};

module.exports = {

  hide: hide,

  show: function (id) {
    hide();

    var modal = document.getElementById('modal-' + id);
    if(!modal)
      throw 'modal "' + id + '" not found';

    modal.classList.add('opened');

    if(id in handlers)
      return;
    handlers[id] = modal.addEventListener('click', function () {
      history.back();
    });
    if(isTouchEnabled)
      modal.addEventListener('touchstart', function () {
        history.back();
      });
  }

};