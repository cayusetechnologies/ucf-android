var KenBurns = function (container, delay) {
  var handle, i = 0, images = container.getElementsByTagName('img');

  this.start = function () {
    container.getElementsByTagName('img')[i++].className = "fx";
    handle = setInterval(function () {
      if(i == images.length)
        i = 0;
      images[i].className = "fx";
      if(i === 0)
        images[images.length - 2].className = "";
      if(i === 1)
        images[images.length - 1].className = "";
      if(i > 1)
        images[i - 2].className = "";
      i++;
      // clearInterval(handle);
    }, delay || 5000);
  };

  this.stop = function () {
    clearInterval(handle);
  }

};


module.exports = KenBurns;