var FastClick = require('../bower_components/fastclick/lib/fastclick');

var page =  require('./page');
var modal = require('./modal');
var main =  require('./route/main');
var map =   require('./route/map');

var isNative = window.location.href.substring(0, 4) === 'file';
var nativeRoutes = [ 'events', 'route' ];

var navigate = function (id, next) {
  return [
    function (extra) {
      if(isNative && nativeRoutes.indexOf(id) > -1) {
        window.location.href = 'native/' + id + (extra ? '#' + extra : '');
        return;
      }
      if(id === 'route') {
        window.location.href = require('../data/routes.json')[extra];
        return;
      }
      console.log('Navigating to ' + id + ': ' + extra);
      modal.hide();
      page.show(id);
      if(next)
        next();
    }
  ];
};

var director = require('../bower_components/director/build/director.min');
window.router = director.Router({
  '/': navigate('main', main),
  '/map': navigate('map', map),
  '/events': navigate('events'),
  '/events/(.*)': navigate('events'),
  '/entertainment': navigate('entertainment'),
  '/admissions': navigate('admissions'),
  '/directions': navigate('directions'),
  '/rodeo': navigate('rodeo'),
  '/parking': navigate('parking'),
  '/route/:id': navigate('route'),
  '/modal/:id': function (id) {
    modal.show(id);
  }
});

var hasLoaded = function () {
  document.body.classList.remove('loading');
  document.body.focus();
};

var isReady = function () {
  document.body.classList.add('ready');
};

if ('addEventListener' in document) {
  document.addEventListener('DOMContentLoaded', function() {
    FastClick(document.body);
  }, false);
}

window.resizeMap = map;

var timestamp = new Date().getTime();
require('./device').ready(function () {
  router.configure({
    // html5history: true
  });
  router.init();
  router.setRoute('/');

  hasLoaded();

  var headers = document.querySelectorAll('article > section:first-of-type > h1:first-of-type');
  for(var i = 0; i < headers.length; ++i)
    headers[i].addEventListener('click', function () {
      history.go(-1);
    });

  if(timestamp - new Date().getTime() < 500) {
    document.getElementById('splash').textContent = 'Welcome!';
    setTimeout(isReady, 900);
  } else
    isReady();
});
