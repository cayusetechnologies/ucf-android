var Hammer = require('../bower_components/hammer.js/hammer.min');
var page = require('./page');

window.stripHeader = function () {
  var header = page.getCurrent().querySelector('section > h1:first-child');
  if(!header) return;
  header.style.display = 'none';
  header.parentNode.style.paddingTop = 0;
  return {
    color: window.getComputedStyle(header, null)['background-color'],
    title: header.textContent || ''
  };
}

module.exports = {

  ready: function (callback) {

    var back = function () {
      history.go(-1);
    }

    window.isTouchEnabled = 'ontouchstart' in window || navigator.msMaxTouchPoints;

    if("ondeviceready" in document) {
      document.addEventListener("deviceready", callback, false);
      document.addEventListener("backbutton", back, false);
    } else {
      window.onload = callback;
    }

  }

}

// if (navigator.notification) {
//   window.alert = function (message) {
//     navigator.notification.alert(message, null, "??", 'OK');
//   };
// }
