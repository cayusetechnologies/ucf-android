var hide = function (el) {
  if(!el.hasAttribute('hidden'))
    el.setAttribute('hidden', 'hidden');
  return el;
};

var show = function (el) {
  if(el.hasAttribute('hidden'))
    el.removeAttribute('hidden');
  return el;
}

var current;

module.exports = {

  getCurrent: function () {
    return current;
  },

  show: function (id) {
    var pages = document.getElementsByTagName('article');
    for(var i = 0; i < pages.length; ++i) hide(pages[i]);
    current = show(document.getElementById('page-' + id));
  }

};