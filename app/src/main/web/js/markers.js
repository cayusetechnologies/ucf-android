module.exports = [

  /* parking */
  { position: ['17%', '8%'],  location: 'parking', sprite: 'car' },
  { position: ['37%', '72%'], location: 'parking', sprite: 'car' },
  { position: ['76%', '6%'],  location: 'parking', sprite: 'car' },

  /* enterance */
  { position: ['12%', '71%'], location: 'admissions', sprite: 'enterance' },
  { position: ['11%', '46%'], location: 'admissions', sprite: 'enterance' },
  { position: ['11%', '19%'], location: 'admissions', sprite: 'enterance' },
  { position: ['87%', '32%'], location: 'admissions', sprite: 'enterance' },
  { position: ['63%', '14%'], location: 'admissions', sprite: 'enterance' },
  { position: ['48%', '87%'], location: 'admissions', sprite: 'enterance' },
  { position: ['85%', '87%'], location: 'admissions', sprite: 'enterance' },

  /* entertainment */
  { position: ['45%', '24%'],   location: 'events/location:stage wildhorse',  sprite: 'music' },
  { position: ['50%', '38.9%'], location: 'events/location:stage coke',       sprite: 'music' },
  { position: ['25%', '54%'],   location: 'events/location:stage les schwab', sprite: 'music' },

  /* livestock */ /*beef is missing*/
  { position: ['68%', '26%'],     location: 'events/tag:goat,swine',   sprite: 'paw' },
  { position: ['62.5%', '38.5%'], location: 'events/tag:small animal', sprite: 'paw' },
  { position: ['73.5%', '38.5%'], location: 'events/tag:lamb',         sprite: 'paw' },
  { position: ['87%', '44%'],        modal: 'livestock-horse',         sprite: 'horse' },

  /* charging station */
  { position: ['36%', '39%'], modal: 'charge', sprite: 'battery' },

  /* concession */
  { position: ['31%', '48.5%'], modal: 'food', sprite: 'food' },
  { position: ['41%', '48.5%'], modal: 'vendor', sprite: 'vendor' },

  /* restroom */
  { position: ['25%', '43%'], modal: 'restroom', sprite: 'restroom' },

  /* rodeo */
  { position: ['67%', '70%'], location: 'rodeo', sprite: 'bronco' },

  /* office */
  { position: ['22%', '19%'], modal: 'office', sprite: 'help' }
    
];