//https://youtu.be/UzLnK2SUVLM?list=WL&t=3355

'use strict';

var fs = require('fs');
var gulp = require('gulp');
var gutil = require('gulp-util');
var watchify = require('watchify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
// var buffer = require('vinyl-buffer');
// var sourcemaps = require('gulp-sourcemaps');
// var order = require("gulp-order");
var debug = require('gulp-debug');
var jade = require('gulp-jade');
var concat = require('gulp-concat');
var minifyHTML = require('gulp-minify-html');
var minifyCSS = require('gulp-minify-html');
var uglify = require('gulp-uglify');
var jsValidate = require('gulp-jsvalidate');

function loadJSON (filename) {
  return JSON.parse(fs.readFileSync(filename));
}

watchify.args.entries = [ './js/index.js' ];
watchify.args.debug = true;
var b = watchify(browserify(watchify.args));
b.on('update', bundle);
b.on('log', gutil.log);

var BUILD_TARGET = '../assets/www';

// javascript
gulp.task('script', bundle);
function bundle() {
  return b.bundle()
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('scripts.js'))
    // .pipe(buffer())
    // .pipe(sourcemaps.init({loadMaps: true}))
    // .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(BUILD_TARGET));
}


// static
gulp.task('static', function () {
  gulp.src(['{fonts,images,data}/**/*', 'css/reset.css'])
    .pipe(debug({title: 'static:'}))
    .pipe(gulp.dest(BUILD_TARGET));
});
gulp.watch('{images,fonts,data}/**/*', ['static']);
gulp.watch('css/reset.css', ['static']);


// templates
gulp.task('templates', function() {
  gulp.src('html/**/*.jade')
    .pipe(debug({title: 'template:'}))
    .pipe(jade({
      locals: {
        year:          2015,
        title:         "Umatilla County Fair",
        entertainment: loadJSON('data/entertainment.json'),
        events:        loadJSON('data/events.json'),
        locations:     loadJSON('data/locations.json'),
        pricing:       loadJSON('data/pricing.json'),
        admin:         loadJSON('data/admin.json')
      }
    }))
    .pipe(minifyHTML({}))
    .pipe(gulp.dest(BUILD_TARGET))
});
gulp.watch('{data,html}/**/*', ['templates']);


// stylesheets
gulp.task('style', function () {
  gulp.src(['!css/reset.css', 'css/**/*'])
    .pipe(debug({title: 'stylesheet:'}))
    .pipe(concat('styles.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest(BUILD_TARGET));
});
gulp.watch('css/**/*', ['style']);


gulp.task('default', ['templates', 'static', 'script', 'style']);