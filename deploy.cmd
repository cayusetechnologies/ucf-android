@echo off

echo Updating Other Devices
start robocopy "app/src/main/assets/www" "\\penws01/apps/ucf/other-devices" /MIR /Z /W:5

echo Building Android
call gradle assembleRelease

echo Copying Android
copy /Y app\build\outputs\apk\app-release.apk \\penws01\apps\ucf\android\ucf-1.1.1.apk
